/*
    Name: Shane Goldsberry
    Date: 09-11-2018
*/

import java.util.Scanner;

public class BasicInput {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int userInt;
        double userDouble;
        char userChar;
        String userString;

        //Get integer from user
        System.out.println("Enter integer:");
        userInt = scnr.nextInt();

        //Get double from user
        System.out.println("Enter double:");
        userDouble = scnr.nextDouble();

        //Get character from user
        System.out.println("Enter character:");
        userChar = scnr.next().charAt(0);

        //Get string from user
        System.out.println("Enter string:");
        userString = scnr.next();

        //Output all variables on a single line
        System.out.println(userInt + " " + userDouble + " " + userChar + " " + userString);

        //Output all variables on a single line, backwards
        System.out.println(userString + " " + userChar + " " + userDouble + " " + userInt);

        //Cast the double to an integer
        System.out.println(userDouble + " cast to an integer is " + (int)(userDouble));
    }
}