/*
   Name: (Enter your name here)
   Date: (Enter date completed here)
*/

import java.util.Scanner;

public class DrawRightTriangle {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        char triangleChar;
        int triangleHeight;
        int i, j;
        String rowStr = "";

        System.out.println("Enter a character:");
        triangleChar = scnr.next().charAt(0);

        System.out.println("Enter triangle height:");
        triangleHeight = scnr.nextInt();
        System.out.println("");

        for (i = 1; i <= triangleHeight; i++) {
            rowStr = "";
            for (j = 1; j <= i; j++) {
                rowStr += triangleChar + " ";
            }
            System.out.println(rowStr);
        }
    }
}