/*
    Name: Shane Goldsberry
    Date: 09/04/18
*/

import java.util.Scanner;

public class OutputWithVars {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int userNum;
        int userNum_sq;
        int userNum_cbd;
        int userNum2;
        int num_sum;
        int num_prod;

        System.out.println("Enter integer:");
        userNum = scnr.nextInt();
        System.out.println("You entered: " + userNum);

        userNum_sq = (userNum*userNum);
        userNum_cbd = (userNum*userNum*userNum);
        System.out.println(userNum + " squared is " + userNum_sq);
        System.out.println("And " + userNum + " cubed is " + userNum_cbd + "!!");

        System.out.println("Enter another integer:");
        userNum2 = scnr.nextInt();

        num_sum = (userNum + userNum2);
        num_prod = (userNum * userNum2);
        System.out.println(userNum + " + " + userNum2 + " is " + num_sum);
        System.out.println(userNum + " * " + userNum2 + " is " + num_prod);
    }
}