/*
    Created By: Shane Goldsberry
    Created On: Oct, 09 2018
*/

import java.util.Scanner;

public class PlayerRoster {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        final int NUM_JERSEYS = 5;
        int[] JersyNumber = new int[NUM_JERSEYS];
        int[] PlayerRating = new int[NUM_JERSEYS];
        String[] jPut = new String[NUM_JERSEYS];
        int i = 0;
        boolean quit = false;
        for (i = 0; i < NUM_JERSEYS; i++) {
            System.out.println("Enter player " + (i + 1) + "'s jersey number:");
            JersyNumber[i] = sc.nextInt();
            System.out.println("Enter player " + (i + 1) + "'s rating:");
            PlayerRating[i] = sc.nextInt();
            System.out.println("");

        }

        System.out.println("ROSTER");
        int N = 0;
        for (N = 0; N < NUM_JERSEYS; N++) {
            System.out.println("Player " + (N + 1) + " -- Jersey number: " + JersyNumber[N] + ", Rating: " + PlayerRating[N]);
        }

        while (!quit) {
            String Stg = "MENU\n"
                    + "u - Update player rating\n"
                    + "a - Output players above a rating\n"
                    + "r - Replace player\n"
                    + "o - Output roster\n"
                    + "q - Quit\n";
            System.out.println("");
            System.out.println(Stg);
            System.out.println("Choose an option:");
            String menuOption = "?";
            menuOption = sc.nextLine();

            boolean correctInput = false;
            if (menuOption.equals("u") || menuOption.equals("a") || menuOption.equals("r") || menuOption.equals("o") || menuOption.equals("q")) {
                correctInput = true;
                menuOption = menuOption.trim();
            } else {
                correctInput = false;
            }

            if (menuOption.equals("u")) {
                System.out.println("Enter jersey number:");
                int jerseyNum = sc.nextInt();

                System.out.println("New rating for player:");
                int newRate = sc.nextInt();
                int M = 0;
                for (M = 0; M < NUM_JERSEYS; M++) {

                    if (JersyNumber[M] == jerseyNum) {
                        PlayerRating[M] = newRate;
                    }
                }
            } else if (menuOption.equals("a")) {
                System.out.println("Enter a rating:");
                int rating = sc.nextInt();
                int k = 0;
                for (k = 0; k < NUM_JERSEYS; k++) {
                    if (PlayerRating[k] > rating) {
                        System.out.println("Player " + (k + 1) + " -- Jersey Number: " + JersyNumber[k] + ", Rating: " + PlayerRating[k]);
                    }
                }

            } else if (menuOption.equals("o")) {
                System.out.println("ROSTER");
                int J = 0;
                for (J = 0; J < NUM_JERSEYS; J++) {
                    System.out.println("Player " + (J + 1) + " -- Jersey number: " + JersyNumber[J] + ", Rating: " + PlayerRating[J]);
                }
            } else if (menuOption.equals("q")) {
                quit = true;
            } else if (menuOption.equals("r")) {
                System.out.println("Enter jersey number:");
                int jerNum = sc.nextInt();
                int l = 0;
                for (l = 0; l < NUM_JERSEYS; l++) {
                    if (JersyNumber[l] == jerNum) {
                        System.out.println("Enter new jersey number:");
                        JersyNumber[l] = sc.nextInt();
                        System.out.println("Enter new player rating:");
                        PlayerRating[l] = sc.nextInt();
                    }
                }
                int a = 0;
                for (a = 0; a < NUM_JERSEYS; a++) {

                    System.out.println("Player " + (a + 1) + " -- Jersey number: " + JersyNumber[a] + ", Rating: " + PlayerRating[a]);
                }

            }

        }

        return;
    }

}