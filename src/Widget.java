/*
    Name: Shane Goldsberry
    Date: Oct 17, 2018

    This application will demonstrate the creation of a collection of objects.
    There will be user input to create individual widget objects that contain
    a main field and a price.

    Team member with branch:

 */

public class Widget {

    // object fields (object variables, members, etc)
    private String name;
    private double price;

    //class fields (class variables that are created even without any objects created)
    private final static int MAX_WIDGETS = 5;
    private static int count = 0;

    //argument-based constructors

    public Widget(String name, double price) {
        //FIXME
    }

    //no-argument constructor
    public Widget() {
        this("Generic", 1.00);
    }

    //object methods
    public String getName() {return name;}
    public double getPrice() {return price;}
    public void setPrice() {//FIXME
    }

    //class methods
    public static int getMaxWidgets() {return MAX_WIDGETS;}
    public static int getCount() {return count;}
    public static void updateCount() {count++;}
}
