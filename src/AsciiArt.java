/*
    Name: Shane Goldsberry
    Date: 09/04/18
*/

public class AsciiArt {
    public static void main(String[] args) {

        //Draw tree
        System.out.println("   *");
        System.out.println("  ***");
        System.out.println(" *****");
        System.out.println("*******");
        System.out.println("  ***");

        System.out.println("");
        System.out.println("");

        //Draw cat
        System.out.println("/\\   /\\");
        System.out.println("  o o");
        System.out.println(" =   =");
        System.out.println("  ---");
    }
}