/*
   Name: Shane Goldsberry
   Date: Oct 02, 2018
   Edited On: Oct 03, 2018
*/

import java.util.Scanner;

public class DrawHalfArrow {
    public static void main(String[] args) {
        Scanner scnr = new Scanner(System.in);
        int arrowBaseHeight;
        int arrowBaseWidth;
        int arrowHeadWidth = 0;
        int row, i;

        System.out.println("Enter arrow base height:");
        arrowBaseHeight = scnr.nextInt();

        System.out.println("Enter arrow base width:");
        arrowBaseWidth = scnr.nextInt();

        do {
            System.out.println("Enter arrow head width:");
            arrowHeadWidth = scnr.nextInt();
        } while (arrowHeadWidth <= arrowBaseWidth);


        System.out.println("");

        for (row = 0; row < arrowBaseHeight; row++) {
            for (i = 0; i < arrowBaseWidth; ++i) {
                System.out.print("*");
            }
            System.out.println("");
        }

        for (row = arrowHeadWidth; row > 0; row--) {
            for (i = 0; i < row; i++) {
                System.out.print("*");
            }
            System.out.println("");
        }

    }
}